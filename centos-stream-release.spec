%global distro  CentOS Stream
%global major   9
%global minor   0

Name:           centos-stream-release
Version:        %{major}.%{minor}
Release:        10%{?dist}
Summary:        %{distro} release files
License:        GPLv2
URL:            https://centos.org
BuildArch:      noarch

Provides:       centos-release = %{version}-%{release}

# Required for a lorax run (to generate install media)
Requires:       centos-stream-repos(%{major})
Provides:       centos-release-eula
Provides:       redhat-release-eula

# required by epel-release
Provides:       redhat-release = %{version}-%{release}

# required by dnf
# https://github.com/rpm-software-management/dnf/blob/4.2.23/dnf/const.py.in#L26
Provides:       system-release = %{version}-%{release}
Provides:       system-release(releasever) = %{major}

# required by libdnf
# https://github.com/rpm-software-management/libdnf/blob/0.48.0/libdnf/module/ModulePackage.cpp#L472
Provides:       base-module(platform:el%{major})

Source200:      EULA
Source201:      LICENSE
Source202:      Contributors

Source300:      85-display-manager.preset
Source301:      90-default.preset
Source302:      90-default-user.preset
Source303:      99-default-disable.preset
Source304:      50-redhat.conf

Source400:      centossecurebootca2.cer
# kernel signing certificate
Source401:      centossecureboot201.cer
# grub2 signing certificate
Source402:      centossecureboot202.cer
# Fwupd signing certificate
Source403:      centossecureboot203.cer

Source500:      centos.repo
Source501:      centos-addons.repo

Source600:      RPM-GPG-KEY-centosofficial
Source601:      RPM-GPG-KEY-CentOS-SIG-Extras


%package -n centos-sb-certs
Summary: %{distro} public secureboot certificates
Group: System Environment/Base
Provides: system-sb-certs = %{version}-%{release}

%package -n centos-stream-repos
Summary:        CentOS Stream package repositories
Provides:       system-repos = %{version}-%{release}
Provides:       centos-stream-repos(%{major}) = %{version}
Requires:       centos-stream-release = %{version}-%{release}
Requires:       centos-gpg-keys = %{version}-%{release}

%package -n centos-gpg-keys
Summary:        CentOS RPM keys


%description
%{distro} release files.

%description -n centos-sb-certs
%{distro} secureboot certificates

%description -n centos-stream-repos
This package provides the package repository files for CentOS Stream.

%description -n centos-gpg-keys
This package provides the RPM signature keys for CentOS.


%install
# copy license and contributors doc here for %%license and %%doc macros
mkdir -p ./docs
cp %{SOURCE201} %{SOURCE202} ./docs

# create /etc/system-release and /etc/redhat-release
install -d -m 0755 %{buildroot}%{_sysconfdir}
echo "%{distro} release %{major}" > %{buildroot}%{_sysconfdir}/centos-release
ln -s centos-release %{buildroot}%{_sysconfdir}/system-release
ln -s centos-release %{buildroot}%{_sysconfdir}/redhat-release

# Create the os-release file
install -d -m 0755 %{buildroot}%{_prefix}/lib
cat > %{buildroot}%{_prefix}/lib/os-release << EOF
NAME="%{distro}"
VERSION="%{major}"
ID="centos"
ID_LIKE="rhel fedora"
VERSION_ID="%{major}"
PLATFORM_ID="platform:el%{major}"
PRETTY_NAME="%{distro} %{major}"
ANSI_COLOR="0;31"
LOGO="fedora-logo-icon"
CPE_NAME="cpe:/o:centos:centos:%{major}"
HOME_URL="https://centos.org/"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
REDHAT_SUPPORT_PRODUCT="Red Hat Enterprise Linux %{major}"
REDHAT_SUPPORT_PRODUCT_VERSION="%{distro}"
EOF

# Create the symlink for /etc/os-release
ln -s ../usr/lib/os-release %{buildroot}%{_sysconfdir}/os-release

# write cpe to /etc/system/release-cpe
echo "cpe:/o:centos:centos:%{major}" > %{buildroot}%{_sysconfdir}/system-release-cpe

# create /etc/issue, /etc/issue.net and /etc/issue.d
echo '\S' > %{buildroot}%{_sysconfdir}/issue
echo 'Kernel \r on an \m' >> %{buildroot}%{_sysconfdir}/issue
cp %{buildroot}%{_sysconfdir}/issue{,.net}
echo >> %{buildroot}%{_sysconfdir}/issue
mkdir -p %{buildroot}%{_sysconfdir}/issue.d

# set up the dist tag macros
mkdir -p %{buildroot}%{_rpmmacrodir}
cat > %{buildroot}%{_rpmmacrodir}/macros.dist << EOF
# dist macros.

%%__bootstrap ~bootstrap
%%centos_ver %{major}
%%centos %{major}
%%rhel %{major}
%%dist %%{!?distprefix0:%%{?distprefix}}%%{expand:%%{lua:for i=0,9999 do print("%%{?distprefix" .. i .."}") end}}.el%{major}%%{?with_bootstrap:%{__bootstrap}}
%%el%{major} 1
EOF

# use unbranded datadir
install -d -m 0755 %{buildroot}%{_datadir}/centos-release
ln -s centos-release %{buildroot}%{_datadir}/redhat-release
install -p -m 0644 %{SOURCE200} %{buildroot}%{_datadir}/centos-release/

# copy systemd presets
install -d -m 0755 %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -d -m 0755 %{buildroot}%{_prefix}/lib/systemd/user-preset
install -p -m 0644 %{SOURCE300} %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -p -m 0644 %{SOURCE301} %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -p -m 0644 %{SOURCE302} %{buildroot}%{_prefix}/lib/systemd/user-preset/

# installing the same file for both system and user presets to set the same behavior for both
install -p -m 0644 %{SOURCE303} %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -p -m 0644 %{SOURCE303} %{buildroot}%{_prefix}/lib/systemd/user-preset/

# copy sysctl presets
mkdir -p %{buildroot}/%{_prefix}/lib/sysctl.d/
install -m 0644 %{SOURCE304} %{buildroot}/%{_prefix}/lib/sysctl.d/

# Create stub yum repos
mkdir %{buildroot}%{_sysconfdir}/yum.repos.d
touch %{buildroot}%{_sysconfdir}/yum.repos.d/redhat.repo

# Copy secureboot certificates
install -d -m 0755 %{buildroot}%{_sysconfdir}/pki/sb-certs/
install -d -m 0755 %{buildroot}%{_datadir}/pki/sb-certs/

# Install aarch64 certs
install -m 644 %{SOURCE400} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-ca-aarch64.cer
install -m 644 %{SOURCE401} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-kernel-aarch64.cer
install -m 644 %{SOURCE402} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-grub2-aarch64.cer
install -m 644 %{SOURCE403} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-fwupd-aarch64.cer

# Install x86_64 certs
install -m 644 %{SOURCE400} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-ca-x86_64.cer
install -m 644 %{SOURCE401} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-kernel-x86_64.cer
install -m 644 %{SOURCE402} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-grub2-x86_64.cer
install -m 644 %{SOURCE403} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-fwupd-x86_64.cer

# Install ppc64le certs
install -m 644 %{SOURCE400} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-ca-ppc64le.cer
install -m 644 %{SOURCE401} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-kernel-ppc64le.cer
install -m 644 %{SOURCE402} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-grub2-ppc64le.cer

# Install s390x certs
install -m 644 %{SOURCE400} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-ca-s390x.cer
install -m 644 %{SOURCE401} %{buildroot}%{_datadir}/pki/sb-certs/secureboot-kernel-s390x.cer

# Link x86_64 certs
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-ca-x86_64.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-ca-x86_64.cer
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-kernel-x86_64.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-kernel-x86_64.cer
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-grub2-x86_64.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-grub2-x86_64.cer
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-fwupd-x86_64.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-fwupd-x86_64.cer

# Link aarch64 certs
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-ca-aarch64.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-ca-aarch64.cer
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-kernel-aarch64.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-kernel-aarch64.cer
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-grub2-aarch64.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-grub2-aarch64.cer
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-fwupd-aarch64.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-fwupd-aarch64.cer

# Link ppc64le certs
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-ca-ppc64le.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-ca-ppc64le.cer
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-kernel-ppc64le.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-kernel-ppc64le.cer
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-grub2-ppc64le.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-grub2-ppc64le.cer

# Link s390x certs
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-ca-s390x.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-ca-s390x.cer
ln -sr %{buildroot}%{_datadir}%{_datadir}/pki/sb-certs/secureboot-kernel-s390x.cer %{buildroot}%{_sysconfdir}/pki/sb-certs/secureboot-kernel-s390x.cer

# copy yum repos
install -d -m 0755 %{buildroot}%{_sysconfdir}/yum.repos.d
install -p -m 0644 %{SOURCE500} %{buildroot}%{_sysconfdir}/yum.repos.d/
install -p -m 0644 %{SOURCE501} %{buildroot}%{_sysconfdir}/yum.repos.d/

# dnf variables
install -d -m 0755 %{buildroot}%{_sysconfdir}/dnf/vars
echo "%{major}-stream" > %{buildroot}%{_sysconfdir}/dnf/vars/stream

# copy GPG keys
install -d -m 0755 %{buildroot}%{_sysconfdir}/pki/rpm-gpg
install -p -m 0644 %{SOURCE600} %{buildroot}%{_sysconfdir}/pki/rpm-gpg/
install -p -m 0644 %{SOURCE601} %{buildroot}%{_sysconfdir}/pki/rpm-gpg/


%files
%license docs/LICENSE
%doc docs/Contributors
%{_sysconfdir}/redhat-release
%{_sysconfdir}/system-release
%{_sysconfdir}/centos-release
%config(noreplace) %{_sysconfdir}/os-release
%config %{_sysconfdir}/system-release-cpe
%config(noreplace) %{_sysconfdir}/issue
%config(noreplace) %{_sysconfdir}/issue.net
%dir %{_sysconfdir}/issue.d
%dir %{_sysconfdir}/yum.repos.d
%ghost %{_sysconfdir}/yum.repos.d/redhat.repo
%{_rpmmacrodir}/macros.dist
%{_datadir}/redhat-release
%{_datadir}/centos-release
%{_prefix}/lib/os-release
%{_prefix}/lib/systemd/system-preset/*
%{_prefix}/lib/systemd/user-preset/*
%{_prefix}/lib/sysctl.d/50-redhat.conf

%files -n centos-sb-certs
# Note to future packagers:
# resetting the symlinks in /etc/pki/sb-certs on upgrade is the intended behavior here
%dir %{_sysconfdir}/pki/sb-certs
%dir %{_datadir}/pki/sb-certs/
%{_sysconfdir}/pki/sb-certs/*.cer
%{_datadir}/pki/sb-certs/*.cer

%files -n centos-stream-repos
%config(noreplace) %{_sysconfdir}/yum.repos.d/centos.repo
%config(noreplace) %{_sysconfdir}/yum.repos.d/centos-addons.repo
%config(noreplace) %{_sysconfdir}/dnf/vars/stream

%files -n centos-gpg-keys
%{_sysconfdir}/pki/rpm-gpg


%changelog
* Wed Feb 02 2022 Stephen Gallagher <sgallagh@redhat.com> - 9.0-10
- Enable switcheroo-control.service
- Resolves: rhbz#2049627

* Sun Jan 30 2022 Neal Gompa <ngompa@datto.com> - 9.0-9
- Restructure centos-stream-release and centos-stream-repos dependencies
- Resolves: rhbz#2048268

* Mon Jan 17 2022 Neal Gompa <ngompa@centosproject.org> - 9.0-8
- Use metalinks for extras-common repos
- Resolves: rhbz#2037871

* Thu Jan 06 2022 Fabian Arrotin <arrfab@redhat.com> - 9.0-7
- Adding Extras-common repos for SIGs produced content
- Resolves: rhbz#2037871

* Mon Dec 13 2021 Neal Gompa <ngompa@centosproject.org> - 9.0-6
- Add LOGO to os-release(5) data
  Resolves: rhbz#2031998

* Thu Nov 18 2021 Timothée Ravier <tim@siosm.fr> - 9.0-5
- Create and own /etc/issue.d directory

* Fri Nov 12 2021 Stephen Gallagher <sgallagh@redhat.com> - 9.0-4
- Add preset to enable WirePlumber by default
- Related: rhbz#2022717

* Mon Oct 04 2021 Carl George <carl@redhat.com> - 9.0-3
- Specify protocols in metalink URLs
- Related: rhbz#2006565

* Tue Sep 21 2021 Carl George <carl@redhat.com> - 9.0-2
- Add repos and gpg-keys subpackages
- Resolves: rhbz#2006565

* Fri Sep 10 2021 Stephen Gallagher <sgallagh@redhat.com> - 9.0-1.0.12
- Ship public secureboot certs in a well-known location
- Related: rhbz#1997343
- Switch to libvirt modular daemons
- Related: rhbz#1991965

* Thu Sep 02 2021 Stephen Gallagher <sgallagh@redhat.com> - 9.0-1.0.11
- Update EULA to refer to CentOS Stream 9 instead of CentOS Linux 8
- Resolves: CS-798

* Wed Sep 01 2021 Stephen Gallagher <sgallagh@redhat.com> - 9.0-1.0.10
- Drop nfs-convert.service preset
- Related: rhbz#1937811

* Fri Aug 06 2021 Stephen Gallagher <sgallagh@redhat.com> - 9.0-1.0.9
- Add stub yum repo for lorax
- Fixes: rhbz#1990671

* Mon Jul 26 2021 Neal Gompa <ngompa@centosproject.org> - 9.0-1.0.8
- Move dist macros from /etc to /usr

* Mon Jul 26 2021 Mikolaj Izdebski <mizdebsk@redhat.com> - 9.0-1.0.7
- Define __bootstrap macro

* Mon Jul 26 2021 Neal Gompa <ngompa@centosproject.org> - 9.0-1.0.6
- Add missing systemd user presets to enable PipeWire services

* Fri Jul 23 2021 Neal Gompa <ngompa@centosproject.org> - 9.0-1.0.5
- Add missing presets for SDDM synced from Fedora

* Mon Apr 05 2021 Brian Stinson <bstinson@redhat.com> - 9.0-1.0.4
- Add some missing systemd presets

* Tue Mar 23 2021 Josh Boyer <jwboyer@redhat.com> - 9.0-1.0.3
- Add sysctl.d presets

* Wed Mar 17 2021 Troy Dawson <tdawson@redhat.com> - 9.0-1.0.2
- Add distprefix to %{dist}

* Fri Mar 12 2021 Troy Dawson <tdawson@redhat.com> - 9.0-1.0.1
- Relax dependency for centos-repos

* Wed Mar 03 2021 Michal Konecny <mkonecny@redhat.com> - 9.0-1
- Update to version 9.0

* Mon Sep 28 2020 Carl George <carl@george.computer> - 8.4-1
- Update to version 8.4

* Wed Sep 02 2020 Carl George <carl@george.computer> - 8.3-1
- Convert to centos-stream-release

* Fri May 15 2020 Pablo Greco <pgreco@centosproject.org> - 8-2.0.1
- Relax dependency for centos-repos
- Remove update_boot, it was never used in 8
- Add rootfs_expand to aarch64
- Bump release for 8.2

* Thu Mar 12 2020 bstinson@centosproject.org - 8-1.0.9
- Add the Devel repo to centos-release
- Install os-release(5) content to /usr/lib and have /etc/os-release be a symlink (ngompa)pr#9

* Thu Jan 02 2020 Brian Stinson <bstinson@centosproject.org> - 8-1.0.8
- Add base module platform Provides so DNF can auto-discover modular platform (ngompa)pr#6
- Switched CR repo to mirrorlist to spread the load (arrfab)pr#5

* Thu Dec 19 2019 bstinson@centosproject.org - 8-1.0.7
- Typo fixes
- Disable the HA repo by default

* Wed Dec 18 2019 Pablo Greco <pgreco@centosproject.org> - 8-1.el8
- Fix requires in armhfp

* Tue Dec 17 2019 bstinson@centosproject.org - 8-1.el8
- Add the HighAvailability repository

* Wed Aug 14 2019 Neal Gompa <ngompa@centosproject.org> 8-1.el8
- Split repositories and GPG keys out into subpackages

* Sat Aug 10 2019 Fabian Arrotin <arrfab@centos.org> 8-0.el8
- modified baseurl paths, even if disabled

* Sat Aug 10 2019 Fabian Arrotin <arrfab@centos.org> 8-0.el8
- Enabled Extras by default.
- Fixed sources paths for BaseOS/AppStream

* Sat Aug 10 2019 Brian Stinson <bstinson@centosproject.org> 8-0.el7
- Update Debuginfo and fasttrack to use releasever
- Fix CentOS-media.repo to include appstream

* Wed May 08 2019 Pablo Greco <pablo@fliagreco.com.ar> 8-0.el7
- Initial setup for CentOS-8
